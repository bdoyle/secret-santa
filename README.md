# Secret Santa

Generates a random secret Santa list from a group of seemingly ordinary people. 
People in the same house will not be matched up as a Santa and gift receiver.

```shell
mix escript.build
./secret_santa -f people.yml
```

## Input File

The input file is a list of people eligible to be a Secret Santa and
a gift receiver. Here is the yaml file format:

```
- 
  name: Joe Person
  house: ravenclaw
- 
  name: Mary Person
  house: ravenclaw
-
  name: Scooter
  house: slytherin
-
  name: Greg Person
  house: slytherin
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/secret_santa](https://hexdocs.pm/secret_santa).

