defmodule CommandLine.CLI do
  def main(args) do
    {opts, _, _} =
      OptionParser.parse(args,
        switches: [file: :string],
        aliases: [f: :file]
      )

    if opts[:file] do
      :yamerl_constr.file(opts[:file])
      |> List.first()
      |> create_santas(1, 10)
      |> print_santas
    end
  end

  # For the given people (yaml), returns a map of santa name -> receiver.
  # If a receiver can't be found for a given Santa, will retry max_attempts.
  # If max_attempts is reached, then return an empty set.
  #
  # - shuffle the list of people
  # - loop over shuffled list find a valid receiver
  # - if all valid receivers return, otherwise retry or return an empty map
  defp create_santas(people, attempt, max_attempts) do
    IO.puts("Attempt #{attempt} of #{max_attempts}")

    santas =
      Enum.shuffle(people)
      |> Enum.reduce(%{}, fn santa, memo ->
        receiver =
          (people -- Map.values(memo))
          |> Enum.find(fn receiver -> is_valid_receiver(santa, receiver) end)

        Map.put(memo, person_value(santa, 'name'), receiver)
      end)

    # there is a chance we might not find a valid receiver for every Santa
    # ensure we have all non-null receivers and return
    cond do
      Enum.all?(Map.values(santas)) -> santas
      attempt < max_attempts -> create_santas(people, attempt + 1, max_attempts)
      true -> %{}
    end
  end

  defp print_santas(santas) do
    Enum.each(santas, fn {santa, receiver} ->
      receiver_name = person_value(receiver, 'name')
      santa_name = String.pad_trailing(santa, 20)
      IO.puts("\tSanta: #{santa_name} Receiver: #{receiver_name}")
    end)

    santas
  end

  # Returns true if the given receiver/santa have different houses, false otherwise.
  defp is_valid_receiver(santa, receiver) do
    person_value(santa, 'house') != person_value(receiver, 'house')
  end

  # get string value from yaml file
  defp person_value(person, value) do
    :proplists.get_value(value, person) |> List.to_string()
  end
end
