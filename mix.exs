defmodule SecretSanta.MixProject do
  use Mix.Project

  def project do
    [
      app: :secret_santa,
      version: "0.1.0",
      elixir: "~> 1.6",
      escript: [main_module: CommandLine.CLI],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :yamerl]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:yamerl, "~> 0.7.0"}
    ]
  end
end
